package com.example.newsreader.Services;

import com.example.newsreader.Models.NYTResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NYTService {
    @GET("home.json?api-key=P9PRGVg8SqpqUxde2OOQebAMhOmBt3Fv")
    Call<NYTResponse> getNews();

}
