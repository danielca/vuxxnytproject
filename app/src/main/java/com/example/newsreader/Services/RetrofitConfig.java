package com.example.newsreader.Services;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitConfig {
    private final Retrofit retrofit;

    public RetrofitConfig() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl("https://api.nytimes.com/svc/topstories/v2/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    public NYTService getNYTService() {
        return this.retrofit.create(NYTService.class);
    }
}
