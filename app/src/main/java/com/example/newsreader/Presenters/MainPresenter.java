package com.example.newsreader.Presenters;

import android.content.Context;
import android.util.Log;

import com.example.newsreader.Listeners.ServiceListener;
import com.example.newsreader.Models.NYTResponse;
import com.example.newsreader.Services.RetrofitConfig;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter {

    public MainPresenter() {
    }

    public void getNews(final ServiceListener listener) {
        Call<NYTResponse> call = new RetrofitConfig().getNYTService().getNews();
        call.enqueue(new Callback<NYTResponse>() {
            @Override
            public void onResponse(Call<NYTResponse> call, Response<NYTResponse> response) {
                if (response != null && response.body() != null) {
                    listener.onServiceSucess(response.body().getResults());
                } else {
                    listener.onServiceFail();
                }
            }

            @Override
            public void onFailure(Call<NYTResponse> call, Throwable t) {
                listener.onServiceFail();
            }
        });
    }

}
