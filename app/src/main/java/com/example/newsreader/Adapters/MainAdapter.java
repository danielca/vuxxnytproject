package com.example.newsreader.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newsreader.Listeners.NewsClickListener;
import com.example.newsreader.Models.Multimedia;
import com.example.newsreader.Models.News;
import com.example.newsreader.R;
import com.example.newsreader.Utils.ImageTypeEnum;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter {

    Context context;
    List<News> allNews;
    NewsClickListener listener;

    public MainAdapter(Context context, List<News> allNews, NewsClickListener listener) {
        this.context = context;
        this.allNews = allNews;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false);
        MainViewHolder viewHolder = new MainViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        News selectedNews = allNews.get(position);
        MainViewHolder viewHolder = (MainViewHolder) holder;

        viewHolder.titleTV.setText(selectedNews.getTitle());
        viewHolder.abstractTV.setText(selectedNews.getShortText());
        viewHolder.sectionTV.setText(selectedNews.getSection());

        for(Multimedia media : selectedNews.getMedia()){
            if(media.getFormat().equals(ImageTypeEnum.threeByTwo.getValue())){
                Picasso.get()
                        .load(media.getUrl())
                        .fit()
                        .centerCrop()
                        .into(viewHolder.backgroundIV);
            }
        }



        viewHolder.container.setOnClickListener(view -> {
            listener.onClickNews(selectedNews.getUrl());
        });

        viewHolder.readButton.setOnClickListener(view -> {
            listener.onWatchedNews(selectedNews);
        });

        viewHolder.readButton.setOnLongClickListener(view -> {
            Snackbar.make(viewHolder.container, R.string.long_click_read_button, Snackbar.LENGTH_SHORT).show();
            return true;
        });
    }

    @Override
    public int getItemCount() {
        if (allNews != null) {
            return allNews.size();
        }
        return 0;
    }


    public static class MainViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ConstraintLayout container;
        public ImageView backgroundIV;
        public TextView sectionTV;
        FloatingActionButton readButton;
        TextView titleTV;
        TextView abstractTV;

        public MainViewHolder(View view) {
            super(view);
            container = view.findViewById(R.id.container);
            backgroundIV = view.findViewById(R.id.backgroundIV);
            sectionTV = view.findViewById(R.id.sectionTV);
            readButton = view.findViewById(R.id.readButton);
            titleTV = view.findViewById(R.id.titleTV);
            abstractTV = view.findViewById(R.id.abstractTV);
        }
    }
}


