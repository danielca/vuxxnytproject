package com.example.newsreader.Listeners;

import com.example.newsreader.Models.News;

import java.util.List;

public interface ServiceListener {
    public void onServiceSucess(List<News> news);
    public void onServiceFail();

}
