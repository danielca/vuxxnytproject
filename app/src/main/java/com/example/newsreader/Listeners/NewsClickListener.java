package com.example.newsreader.Listeners;

import com.example.newsreader.Models.News;

public interface NewsClickListener {
    public void onClickNews(String url);
    public void onWatchedNews(News watchedNews);
}
