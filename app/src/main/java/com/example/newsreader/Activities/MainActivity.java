package com.example.newsreader.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.example.newsreader.Adapters.MainAdapter;
import com.example.newsreader.Listeners.NewsClickListener;
import com.example.newsreader.Listeners.ServiceListener;
import com.example.newsreader.Models.News;
import com.example.newsreader.Presenters.MainPresenter;
import com.example.newsreader.R;
import com.example.newsreader.Utils.TinyDB;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements ServiceListener, NewsClickListener {

    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycleView)
    RecyclerView recyclerView;

    @BindView(R.id.emptyTV)
    TextView emptyTV;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    MainPresenter presenter;
    ArrayList<News> allNews;
    ArrayList<News> showNews;

    HashSet<News> readNews;
    MainAdapter adapter;
    TinyDB tinydb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        tinydb = new TinyDB(this);
        presenter = new MainPresenter();

        readNews = new HashSet<>(tinydb.getListObject(getString(R.string.save_read_news), News.class));
        allNews =  tinydb.getListObject(getString(R.string.save_all_news), News.class);

        showNews = new ArrayList<>();

        setupViews();
    }

    public void setupViews() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            presenter.getNews(MainActivity.this);
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MainAdapter(this, showNews, this);
        recyclerView.setAdapter(adapter);

        adjustShowList();

    }

    public void adjustShowList() {
        showNews.clear();
        for (News item : allNews) {
            if (!readNews.contains(item)) {
                showNews.add(item);
            }
            if (showNews.size() == 20) {
                break;
            }
        }
        if(showNews.size() == 0){
            emptyTV.setVisibility(View.VISIBLE);
        }else{
            emptyTV.setVisibility(View.GONE);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStop() {
        super.onStop();
        tinydb.putListObject(getString(R.string.save_all_news), allNews);
        tinydb.putListObject(getString(R.string.save_read_news), new ArrayList<>(readNews));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.reset_read) {
            Snackbar.make(swipeRefreshLayout, R.string.click_reset_button, Snackbar.LENGTH_SHORT).show();
            readNews.clear();
            adjustShowList();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onServiceSucess(List<News> news) {
        allNews.clear();
        allNews.addAll(news);
        adjustShowList();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onServiceFail() {
        Snackbar.make(swipeRefreshLayout, R.string.error_message, Snackbar.LENGTH_SHORT).show();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onClickNews(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    public void onWatchedNews(News watchedNews) {
        readNews.add(watchedNews);
        Snackbar.make(swipeRefreshLayout, getString(R.string.click_read_button), Snackbar.LENGTH_LONG).setAction(R.string.undo, view -> {
            readNews.remove(watchedNews);
            adjustShowList();
        }).show();
        adjustShowList();
    }
}