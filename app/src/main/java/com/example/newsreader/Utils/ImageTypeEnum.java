package com.example.newsreader.Utils;

public enum ImageTypeEnum {

    threeByTwo("mediumThreeByTwo210"),
    superJumbo("superJumbo"),
    normal("Normal"),
    thumbLarge("thumbLarge"),
    thumbnail("Standard Thumbnail");

    public String value;
    ImageTypeEnum(String value) {
        this.value = value;
    }

    public String getValue(){
        return value;
    }


}
