package com.example.newsreader.Models;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class News {
    @JsonProperty("title")
    private String title;
    @JsonProperty("abstract")
    private String shortText;
    @JsonProperty("short_url")
    private String url;
    @JsonProperty("section")
    private String section;
    @JsonProperty("multimedia")
    private List<Multimedia> media;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Multimedia> getMedia() {
        return media;
    }

    public void setMedia(List<Multimedia> media) {
        this.media = media;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result
                + url.hashCode();

        return result;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        News cmp = (News) obj;

        return cmp.getUrl().equals(this.url);

    }
}
